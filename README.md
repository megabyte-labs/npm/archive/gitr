<!-- ⚠️ This README has been generated from the file(s) "./.modules/docs/blueprint-readme.md" ⚠️--><div align="center">
  <center>
    <a href="https://gitlab.com/megabyte-labs/npm/gitr" title="@megabytelabs/gitr GitLab page" target="_blank">
      <img width="100" height="100" alt="@megabytelabs/gitr logo" src="https://gitlab.com/megabyte-labs/npm/gitr/-/raw/master/logo.png" />
    </a>
  </center>
</div>
<div align="center">
  <center><h1 align="center">NPM Package: Gitr</h1></center>
</div>

<div align="center">
  <h4 align="center">
    <a href="https://megabyte.space" title="Megabyte Labs homepage" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/home-solid.svg" />
    </a>
    <a href="https://www.npmjs.com/package/@megabytelabs/gitr" title="@megabytelabs/gitr package on npmjs.org" target="_blank">
      <img height="50" src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/npm.svg" />
    </a>
    <a href="https://gitlab.com/megabyte-labs/npm/gitr/-/blob/master/CONTRIBUTING.md" title="Learn about contributing" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/contributing-solid.svg" />
    </a>
    <a href="https://www.patreon.com/ProfessorManhattan" title="Support us on Patreon" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/support-solid.svg" />
    </a>
    <a href="https://app.slack.com/client/T01ABCG4NK1/C01NN74H0LW/details/" title="Slack chat room" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/chat-solid.svg" />
    </a>
    <a href="https://github.com/ProfessorManhattan/npm-gitr" title="GitHub mirror" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/github-solid.svg" />
    </a>
    <a href="https://gitlab.com/megabyte-labs/npm/gitr" title="GitLab repository" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/gitlab-solid.svg" />
    </a>
  </h4>
  <p align="center">
    <a href="https://www.npmjs.com/package/@megabytelabs/gitr" target="_blank">
      <img alt="Version: 1.0.4" src="https://img.shields.io/badge/version-1.0.4-blue.svg?cacheSeconds=2592000&style=badge_style" />
    </a>
    <a href="https://gitlab.com/megabyte-labs/npm/gitr/commits/master" title="GitLab CI build status" target="_blank">
      <img alt="Build status" src="https://gitlab.com/megabyte-labs/npm/gitr/badges/master/pipeline.svg">
    </a>
    <a href="https://www.npmjs.com/package/@megabytelabs/gitr" title="Dependency status reported by Depfu">
      <img alt="Dependency status reported by Depfu" src="https://img.shields.io/depfu/megabyte-labs/npm-gitr?style=badge_style&logo=npm" />
    </a>
    <a href="https://www.npmjs.com/package/@megabytelabs/gitr" title="Zip file size">
      <img alt="Zip file size" src="https://img.shields.io/bundlephobia/minzip/@megabytelabs/gitr?style=bad_style&logo=npm" />
    </a>
    <a href="" title="Total downloads of @megabytelabs/gitr on npmjs.org">
      <img alt="Total downloads of @megabytelabs/gitr on npmjs.org" src="https://img.shields.io/npm/dt/@megabytelabs/gitr?logo=npm&style=badge_style&logo=npm" />
    </a>
    <a href="https://www.npmjs.com/package/@megabytelabs/gitr" title="Number of vulnerabilities from Snyk scan on @megabytelabs/gitr">
      <img alt="Number of vulnerabilities from Snyk scan on @megabytelabs/gitr" src="https://img.shields.io/snyk/vulnerabilities/npm/@megabytelabs/gitr?style=badge_style&logo=npm" />
    </a>
    <a href="https://megabyte.space/docs/npm" target="_blank">
      <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg?logo=readthedocs&style=badge_style" />
    </a>
    <a href="https://gitlab.com/megabyte-labs/npm/gitr/-/raw/master/LICENSE" target="_blank">
      <img alt="License: MIT" src="https://img.shields.io/badge/license-MIT-yellow.svg?style=badge_style" />
    </a>
    <a href="https://opencollective.com/megabytelabs" title="Support us on Open Collective" target="_blank">
      <img alt="Open Collective sponsors" src="https://img.shields.io/opencollective/sponsors/megabytelabs?logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgBAMAAACBVGfHAAAAElBMVEUAAACvzfmFsft4pfD////w+P9tuc5RAAAABHRSTlMAFBERkdVu1AAAAFxJREFUKM9jgAAXIGBAABYXMHBA4yNEXGBAAU2BMz4FIIYTNhtFgRjZPkagFAuyAhGgHAuKAlQBCBtZB4gzQALoDsN0Oobn0L2PEUCoQYgZyOjRQFiJA67IRrEbAJImNwFBySjCAAAAAElFTkSuQmCC&label=Open%20Collective%20sponsors&style=badge_style" />
    </a>
    <a href="https://github.com/ProfessorManhattan" title="Support us on GitHub" target="_blank">
      <img alt="GitHub sponsors" src="https://img.shields.io/github/sponsors/ProfessorManhattan?label=GitHub%20sponsors&logo=github&style=badge_style" />
    </a>
    <a href="https://github.com/ProfessorManhattan" target="_blank">
      <img alt="GitHub: ProfessorManhattan" src="https://img.shields.io/github/followers/ProfessorManhattan?style=social" target="_blank" />
    </a>
    <a href="https://twitter.com/MegabyteLabs" target="_blank">
      <img alt="Twitter: MegabyteLabs" src="https://img.shields.io/twitter/url/https/twitter.com/MegabyteLabs.svg?style=social&label=Follow%20%40MegabyteLabs" />
    </a>
  </p>
</div>

> </br><h3 align="center">**Creates a GitLab and GitHub repository and then sets up push updating from GitLab to GitHub.**</h3></br>

<!--TERMINALIZER![terminalizer_title](https://gitlab.com/megabyte-labs/npm/role_name/-/raw/master/.demo.gif)TERMINALIZER-->

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#table-of-contents)

## ➤ Table of Contents

- [➤ Requirements](#-requirements)
- [➤ Overview](#-overview)
- [➤ Getting Started](#-getting-started)
- [➤ Example Usage](#-example-usage)
  - [Populating the Role Dependency Chart for the Main Playbook](#populating-the-role-dependency-chart-for-the-main-playbook)
    - [Example Output](#example-output)
  - [Populating the Role Dependency Chart for Individual Roles](#populating-the-role-dependency-chart-for-individual-roles)
  - [Automatically Updating `platforms:` in `galaxy_info:`](#automatically-updating-platforms-in-galaxy_info)
    - [Example Output](#example-output-1)
- [➤ Contributing](#-contributing)
- [➤ License](#-license)

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#requirements)

## ➤ Requirements

- **[Node.js >9](https://gitlab.com/megabyte-labs/ansible-roles/nodejs)**

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#overview)

## ➤ Overview

Ansibler is a set of tools used to manage Ansible playbooks and roles. It provides the following functionality:

- **[Populating the Role Dependency Chart for the Main Playbook](#populating-the-role-dependency-chart-for-the-main-playbook):** Scans through the `roles/` folder and extracts the description of each role from the `meta/main.yml` file to a JSON file (that can be used to display a dependency chart similar to the one found in the [Playr repository](https://gitlab.com/ProfessorManhattan/Playbooks)). This task also injects the project build status and icons representing which operating systems a role supports into the JSON.
- **[Populating the Role Dependency Chart for Individual Roles](#populating-the-role-dependency-chart-for-individual-roles):** You can also create a dependency chart at the role-level as well. Instead of scanning through the `roles/` folder, it will look at the `requirements.yml` folder and generate the JSON that can be used to generate a role-dependency chart for roles that have dependencies.
- **[Automatically Updating `platforms:` in `galaxy_info:`](#automatically-updating-platforms-in-galaxy_info):** When used in combination with [Ansible Molecule JSON](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json), it can automatically populate the `platforms:` section in the `galaxy_info:` of the `meta/main.yml` file found in each role.

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#getting-started)

## ➤ Getting Started

You can run `ansibler` by installing it globally (i.e. `npm install -g @megabytelabs/ansibler`) or running it with `npx`. You can then run `ansibler --help` to see the available parameters. You should see output that looks something like this:

```shell
❯ ansibler --help
Usage: ansibler [options]

  -h, --help            Displays help
  -d, --data String     Relative path to the data file.
  -o, --output String   Relative path to the output file - default: ./ansible-molecule.json
  -c, --command String  Additional commands to run before analyzing
  -i, --inject String   Inject output to a JSON file
```

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#example-usage)

## ➤ Example Usage

Ansibler has several different features that can be individually run or run all at the same time.

### Populating the Role Dependency Chart for the Main Playbook

To generate the role dependency chart (that you can see in the README of our [Playr repository](https://gitlab.com/ProfessorManhattan/Playbooks)), you should:

1. Make sure all the roles are located in the `roles/` folder of your project and that each role has a valid description.
2. Run `ansibler --roles "./roles" --output "./role-chart.json" --populate-descriptions`

The command above will scan through the `roles/` folder and extract all of the descriptions into a JSON file. It might be important to know that a special format is used that is meant to be compatible with [@appnest/readme](https://github.com/andreasbm/readme) which is the documentation generator we use.

#### Example Output

After running the above command (in step 2 above), you will be left with a file titled `role-chart.json` in the current working directory that might look something like this:

```json
{
  "role_dependencies": [
    ["Role Dependency", "Description", "Supported OSes"],
    [
      "<a href='https://gitlab.com/megabyte-space/ansible-roles/androidstudio'>professormanhattan.androidstudio</a>",
      "Installs Android Studio on nearly any OS",
      "Supported OS feature not part of this task"
    ],
    [
      "<a href='https://gitlab.com/megabyte-space/ansible-roles/appium'>professormanhattan.appium</a>",
      "Installs Appium on almost any platform",
      "Supported OS feature not part of this task"
    ],
    [
      "<a href='https://gitlab.com/megabyte-space/ansible-roles/autokey'>professormanhattan.autokey</a>",
      "Installs AutoKey on Linux or AutoHotkey on Windows",
      "Supported OS feature not part of this task"
    ]
  ]
}
```

### Populating the Role Dependency Chart for Individual Roles

You can also generate a role dependency chart for individual roles. Instead of scanning the `roles/` folder to find roles, it looks at the `requirements.yml`. For each role in the `requirements.yml` file, it will include a line in the dependency chart. The chart includes all of the same details as the main playbook dependency chart.

### Automatically Updating `platforms:` in `galaxy_info:`

Every role that is published to Ansible Galaxy needs to include a file in `meta/main.yml` that Ansible Galaxy can use to display your role to the right users. One way the Ansible Galaxy `meta/main.yml` file does this is by providing a section in the file called `platforms:` where information can be populated to tell Ansible Galaxy which operating systems your role will work on. For hundreds of roles, this can be a tedious process.

Many of the roles available on Ansible Galaxy haphazardly list a long list of platforms which the role obviously was not tested on. That does not cut it for us. To address this issue, we developed an automated system for properly generating the `platforms:` compatibility chart in `meta/main.yml`. You can get your `platforms:` variable automatically populated the right way by using our two-step system:

1. First, follow the instructions provided by our [Ansible Molecule JSON](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json) project to set your role(s) up with automated testing
2. With the JSON data that Ansible Molecule JSON generates, you can automatically populate the `meta/main.yml` `platforms:` section by running `ansibler TODO`

#### Example Output

After running the command (in step 2 above), you should be able to see a _proper_ `platforms:` section in any of the roles you generated the compatibility data for. The `platforms:` section of the `meta/main.yml` file in each role should look something like this:

```yaml

```

The Ansible community is really in need of something like this. The `platforms:` section should provide useful data instead of just saying that the role supports all versions of every operating system which is the case way too often.

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#contributing)

## ➤ Contributing

Contributions, issues, and feature requests are welcome! Feel free to check the [issues page](https://gitlab.com/megabyte-labs/npm/gitr/-/issues). If you would like to contribute, please take a look at the [contributing guide](https://gitlab.com/megabyte-labs/npm/gitr/-/blob/master/CONTRIBUTING.md).

<details>
<summary>Sponsorship</summary>
<br/>
<blockquote>
<br/>
I create open source projects out of love. Although I have a job, shelter, and as much fast food as I can handle, it would still be pretty cool to be appreciated by the community for something I have spent a lot of time and money on. Please consider sponsoring me! Who knows? Maybe I will be able to quit my job and publish open source full time.
<br/><br/>Sincerely,<br/><br/>

**_Brian Zalewski_**<br/><br/>

</blockquote>

<a href="https://www.patreon.com/ProfessorManhattan">
  <img src="https://c5.patreon.com/external/logo/become_a_patron_button@2x.png" width="160">
</a>

</details>

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#license)

## ➤ License

Copyright © 2021 [Megabyte LLC](https://megabyte.space). This project is [MIT](https://gitlab.com/megabyte-labs/npm/gitr/-/raw/master/LICENSE) licensed.
