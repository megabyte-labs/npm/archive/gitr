export const CLI_OPTIONS = {
  append: 'Version 1.0.3',
  options: [
    {
      alias: 'h',
      description: 'Displays help',
      option: 'help',
      type: 'Boolean',
    },
    {
      alias: 'c',
      default: './gitr.json',
      description: 'Relative path to the configuration file.',
      example: 'gitr --config ./girt.json',
      option: 'config',
      type: 'String',
    },
  ],
  prepend: 'Usage: gitr [options]',
}
