/**
 * Group data object definition
 */
export type IGroupConfig = {
  readonly parent_id?: number
  readonly visibility?: string
}

/**
 * GitHubURL data object definition
 */
export type IGitHubURLs = {
  readonly withAuth: string
  readonly withoutAuth: string
}

/**
 * Any object definition
 */
export type ILooseObject = {
  readonly [key: string]: unknown
}

export type GitHubPathData = {
  readonly repo: string
  readonly path: string
  readonly owner?: string
}

export type GitHubRepoOwner = {
  readonly login: string
}

export type Project = {
  readonly id: number
  readonly ssh_url_to_repo: string
}

export type Group = {
  readonly id: number
  readonly full_path: string
}

export type RemoteMirror = {
  readonly id: number
  readonly enabled: boolean
  readonly url: string
}

export type Repository = {
  readonly full_name: string
  readonly owner: GitHubRepoOwner
}
