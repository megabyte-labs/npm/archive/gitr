/**
 * Git lab project configurations
 */
export type IProjectConfig = {
  readonly name: string
  readonly description: string
  readonly path: string
  readonly visibility: string
  readonly wiki_enabled: boolean
  readonly namespace_id?: number
}
