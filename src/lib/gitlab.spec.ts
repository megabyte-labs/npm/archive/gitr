/* eslint-disable functional/no-loop-statement */
/* eslint-disable no-magic-numbers */
import test from 'ava'
import fetchMock from 'fetch-mock'

import { GitrOptions } from '../models/gitr-options.model'
import { ILooseObject, Project, Repository } from '../models/object-interfaces'

import { GitLab } from './gitlab'

/**
 * Test failures
 */
test('Testing parsePath failures', (t) => {
  for (const text of ['', '/']) {
    const error = t.throws(
      () => {
        GitLab.parsePath(text)
      },
      { instanceOf: Error }
    )
    t.is(error.message, 'path contains empty segments')
  }

  for (const text of ['gitlab/', '/', '//']) {
    const error = t.throws(
      () => {
        GitLab.parsePath(text)
      },
      { instanceOf: Error }
    )
    t.is(error.message, 'path contains empty segments')
  }

  for (const text of ['/project']) {
    const error = t.throws(
      () => {
        GitLab.parsePath(text)
      },
      { instanceOf: Error }
    )
    t.is(error.message, 'path contains empty segments')
  }

  const error = t.throws(
    () => {
      GitLab.parsePath(
        'group/subgroup1/subgroup2/subgroup3/subgroup4/subgroup5/subgroup6/subgroup7/subgroup8/subgroup9/subgroup10/subgroup11/subgroup12/subgroup13/subgroup14/subgroup15/subgroup16/subgroup17/subgroup18/subgroup19/subgroup20/project'
      )
    },
    { instanceOf: Error }
  )
  t.is(error.message, 'GitLab supports only maximum 20 nested groups')
})

/**
 * Test success
 */
test('Test parsePath success', (t) => {
  t.deepEqual(GitLab.parsePath('username/project'), {
    groupPath: 'username',
    groups: ['username'],
    repoSlug: 'project',
  })
  t.deepEqual(GitLab.parsePath('group/subgroup/project'), {
    groupPath: 'group/subgroup',
    groups: ['group', 'subgroup'],
    repoSlug: 'project',
  })
  t.deepEqual(
    GitLab.parsePath(
      'group/subgroup1/subgroup2/subgroup3/subgroup4/subgroup5/subgroup6/subgroup7/subgroup8/subgroup9/project'
    ),
    {
      groupPath: 'group/subgroup1/subgroup2/subgroup3/subgroup4/subgroup5/subgroup6/subgroup7/subgroup8/subgroup9',
      groups: [
        'group',
        'subgroup1',
        'subgroup2',
        'subgroup3',
        'subgroup4',
        'subgroup5',
        'subgroup6',
        'subgroup7',
        'subgroup8',
        'subgroup9',
      ],
      repoSlug: 'project',
    }
  )
  t.deepEqual(
    GitLab.parsePath(
      'group/subgroup1/subgroup2/subgroup3/subgroup4/subgroup5/subgroup6/subgroup7/subgroup8/subgroup9/subgroup10/subgroup11/subgroup12/subgroup13/subgroup14/subgroup15/subgroup16/subgroup17/subgroup18/subgroup19/project'
    ),
    {
      groupPath:
        'group/subgroup1/subgroup2/subgroup3/subgroup4/subgroup5/subgroup6/subgroup7/subgroup8/subgroup9/subgroup10/subgroup11/subgroup12/subgroup13/subgroup14/subgroup15/subgroup16/subgroup17/subgroup18/subgroup19',
      groups: [
        'group',
        'subgroup1',
        'subgroup2',
        'subgroup3',
        'subgroup4',
        'subgroup5',
        'subgroup6',
        'subgroup7',
        'subgroup8',
        'subgroup9',
        'subgroup10',
        'subgroup11',
        'subgroup12',
        'subgroup13',
        'subgroup14',
        'subgroup15',
        'subgroup16',
        'subgroup17',
        'subgroup18',
        'subgroup19',
      ],
      repoSlug: 'project',
    }
  )
})

const getGitLabProjectData = (): Project => ({
  id: 123,
  ssh_url_to_repo: 'git@example.com:diaspora/diaspora-client.git',
})

const getGitHubProjectData = (): Repository => ({
  full_name: 'owner/repo',
  owner: {
    login: 'owner',
  },
})

const getGitrOptions = (): GitrOptions =>
  new GitrOptions({
    description: 'Description',
    githubPath: 'owner/repo',
    githubToken: 'github token',
    gitlabPath: 'username/project',
    gitlabTitle: 'gitlab title',
    gitlabToken: 'gitlab token',
    mirror: false,
    public: false,
    wiki: false,
  })

const getMirrorListData = ({ id = 101_486, enabled = true, shortUrl = 'github.com/owner/repo.git' }): ILooseObject => ({
  enabled,
  id,
  keep_divergent_refs: true,
  // eslint-disable-next-line unicorn/no-null
  last_error: null,
  last_successful_update_at: '2020-01-06T17:32:02.823Z',
  last_update_at: '2020-01-06T17:32:02.823Z',
  last_update_started_at: '2020-01-06T17:31:55.864Z',
  only_protected_branches: true,
  update_status: 'finished',
  url: `https://*****:*****@${shortUrl}`,
})

test.serial('test setupMirror create mirror entry and enable it', async (t) => {
  const gitLabProjectData = getGitLabProjectData()
  const gitHubProjectData = getGitHubProjectData()
  const options = getGitrOptions()

  const mirrorList = `https://gitlab.com/api/v4/projects/${gitLabProjectData.id}/remote_mirrors`
  const mocked = fetchMock.get(mirrorList, [])
  const mocked1 = fetchMock.post(mirrorList, getMirrorListData({ id: 101_486 }))
  const mocked2 = fetchMock.put(`${mirrorList}/101486`, { id: 321 })
  await GitLab.setMirror(gitLabProjectData, gitHubProjectData, options)
  t.true(mocked.called(mirrorList))
  t.true(mocked1.called(mirrorList))
  t.true(mocked2.called(mirrorList))
  fetchMock.reset()
})

test.serial('test setupMirror other mirrors exists', async (t) => {
  const gitLabProjectData = getGitLabProjectData()
  const gitHubProjectData = getGitHubProjectData()
  const options = getGitrOptions()

  const mirrorList = `https://gitlab.com/api/v4/projects/${gitLabProjectData.id}/remote_mirrors`
  const mocked = fetchMock.get(mirrorList, [getMirrorListData({ id: 101_486, shortUrl: 'github.com/foo/bar.git' })])
  const mocked1 = fetchMock.post(mirrorList, getMirrorListData({ id: 101_486 }))
  const mocked2 = fetchMock.put(`${mirrorList}/101486`, { id: 321 })
  await GitLab.setMirror(gitLabProjectData, gitHubProjectData, options)
  t.true(mocked.called(mirrorList))
  t.true(mocked1.called(mirrorList))
  t.true(mocked2.called(mirrorList))
  fetchMock.reset()
})

test.serial('test setupMirror exists but disabled', async (t) => {
  const gitLabProjectData = getGitLabProjectData()
  const gitHubProjectData = getGitHubProjectData()
  const options = getGitrOptions()

  const mirrorList = `https://gitlab.com/api/v4/projects/${gitLabProjectData.id}/remote_mirrors`
  const mocked = fetchMock.get(mirrorList, [getMirrorListData({ enabled: false, id: 101_486 })])
  const mocked2 = fetchMock.put(`${mirrorList}/101486`, { id: 333 })

  await GitLab.setMirror(gitLabProjectData, gitHubProjectData, options)
  t.true(mocked.called(mirrorList))
  t.true(mocked2.called(mirrorList))
  fetchMock.reset()
})

test.serial('test setupMirror exists and enabled', async (t) => {
  const gitLabProjectData = getGitLabProjectData()
  const gitHubProjectData = getGitHubProjectData()
  const options = getGitrOptions()

  const mirrorList = `https://gitlab.com/api/v4/projects/${gitLabProjectData.id}/remote_mirrors`
  const mocked = fetchMock.get(mirrorList, [getMirrorListData({ enabled: true, id: 101_486 })])

  t.true(await GitLab.setMirror(gitLabProjectData, gitHubProjectData, options))
  t.true(mocked.called(mirrorList))
  fetchMock.reset()
})

test.serial('removeMirrors no existing mirrors', async (t) => {
  const gitLabProjectData = getGitLabProjectData()
  const options = getGitrOptions()

  const mirrorList = `https://gitlab.com/api/v4/projects/${gitLabProjectData.id}/remote_mirrors`
  const mocked = fetchMock.get(mirrorList, [])

  t.true(await GitLab.removeMirror(gitLabProjectData, options))
  t.true(mocked.called(mirrorList))
  fetchMock.reset()
})

test.serial('removeMirrors existing other mirrors', async (t) => {
  const gitLabProjectData = getGitLabProjectData()
  const options = getGitrOptions()

  const mirrorList = `https://gitlab.com/api/v4/projects/${gitLabProjectData.id}/remote_mirrors`
  const mocked = fetchMock.get(mirrorList, [getMirrorListData({ id: 101_486, shortUrl: 'github.com/foo/bar.git' })])

  t.true(await GitLab.removeMirror(gitLabProjectData, options))
  t.true(mocked.called(mirrorList))
  fetchMock.reset()
})

test.serial('removeMirrors existing mirror with disabled', async (t) => {
  const gitLabProjectData = getGitLabProjectData()
  const options = getGitrOptions()

  const mirrorList = `https://gitlab.com/api/v4/projects/${gitLabProjectData.id}/remote_mirrors`
  const mocked = fetchMock.get(mirrorList, [getMirrorListData({ enabled: false, id: 101_486 })])

  t.true(await GitLab.removeMirror(gitLabProjectData, options))
  t.true(mocked.called(mirrorList))
  fetchMock.reset()
})

test.serial('removeMirrors existing mirror with enabled', async (t) => {
  const gitLabProjectData = getGitLabProjectData()
  const options = getGitrOptions()

  const mirrorList = `https://gitlab.com/api/v4/projects/${gitLabProjectData.id}/remote_mirrors`
  const mocked = fetchMock.get(mirrorList, [getMirrorListData({ enabled: true, id: 101_486 })])
  const mockedPUT = fetchMock.put(`${mirrorList}/101486`, getMirrorListData({ enabled: false, id: 10_486 }))

  t.true(await GitLab.removeMirror(gitLabProjectData, options))
  t.true(mocked.called(mirrorList))
  t.true(mockedPUT.called(mirrorList))
  fetchMock.reset()
})
