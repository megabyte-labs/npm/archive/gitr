import { Gitlab } from '@gitbeaker/node'
import 'isomorphic-fetch'

import { GitLabPathData } from '../models/gitlab-path-data'
import { IProjectConfig } from '../models/gitlab-project-config'
import { GitrOptions } from '../models/gitr-options.model'
import { Group, IGroupConfig, ILooseObject, Project, RemoteMirror, Repository } from '../models/object-interfaces'

import { GitHub } from './github'
import { Logger } from './log'

/**
 * GitLab helper
 */
export class GitLab {
  constructor(token: string) {
    this.api = new Gitlab({ token })
  }
  private readonly api

  /**
   * Parse `gitlabPath` from the configuration
   *
   * It will return parsed repository slug, group, subgroup, full groups path and username.
   * If there are only 2 path segments (eg abc/pqr) first path segment will consider as username
   * while other will consider as the repository slug.
   * If there are more than 2 path segments then all the path segments will consider as groups
   * GitLab only supports up to 20 maximum nested groups.
   *
   * @param {string} filePath File path
   * @returns {GitLabPathData} Returns GitLab path data
   */
  public static parsePath(filePath: string): GitLabPathData {
    const elements = filePath.split('/')
    if (elements.includes('')) {
      throw new Error('path contains empty segments')
    }

    // eslint-disable-next-line functional/immutable-data
    const repoSlug = elements.pop()
    // is empty string?
    if (!repoSlug) {
      throw new Error('path contains empty repository slug')
    }

    const groupPath = elements.join('/')
    const maxGroupCount = 20
    if (elements.length > maxGroupCount) {
      throw new Error(`GitLab supports only maximum ${maxGroupCount} nested groups`)
    }

    return {
      groupPath,
      groups: elements,
      repoSlug,
    }
  }

  /**
   * Set push GitHub repository as the GitLab projects push mirror.
   *
   * If there's already GitLab mirror setup to the given GitHub repository and inactive, It will
   * enable the mirror instead of creating a new push mirror.
   *
   * @param {Project} gitlabProject GitLab project data
   * @param {Repository} githubProject GitHub repository data
   * @param {GitrOptions} config Gitr configurations
   * @returns {Promise<boolean>} Returns status of the mirror setup
   */
  public static async setMirror(
    gitlabProject: Project,
    githubProject: Repository,
    config: GitrOptions
  ): Promise<boolean> {
    Logger.info('GitLab: Setting up push mirror to the GitHub')
    const projectId = gitlabProject.id

    // get GitHub urls
    const githubURLs = GitHub.getGitHubMirrorURLs(
      githubProject.owner.login,
      config.githubToken,
      githubProject.full_name
    )

    // get headers for dealing with GitLab mirror endpoints
    const headers = GitLab.getHeaders(config)

    // has mirrors setup already?
    const mirrors = await GitLab.getMirrors(projectId, headers)
    if (mirrors.length > 0) {
      const mirror = mirrors.find((mirrorInfo) => mirrorInfo.url.includes(githubURLs.withoutAuth))
      if (mirror) {
        if (mirror.enabled) {
          Logger.warn('GitLab: GitHub push mirror already setup and enabled')

          return true
        }
        Logger.warn('GitLab: GitHub push mirror already setup but disabled')

        return GitLab.enableMirror(projectId, mirror.id, headers)
      }
    }

    const body = { url: githubURLs.withAuth }
    const response = await fetch(`https://gitlab.com/api/v4/projects/${projectId}/remote_mirrors`, {
      body: JSON.stringify(body),
      headers,
      method: 'POST',
    })
    const mirrorData = ((await response.json()) as unknown) as RemoteMirror

    // Enable the mirror repository
    return GitLab.enableMirror(projectId, mirrorData.id, headers)
  }

  /**
   * Disable the push mirror trigger from the gitlab.com
   *
   * If there's push mirror trigger for the GitLab project, it will safely disable it.
   *
   * @param {Project} gitlabRepo GitLab Project data
   * @param {GitrOptions} config Gitr options
   * @returns {Promise<boolean>} Returns true no mirror or mirror has been removed. Otherwise returns false
   */
  public static async removeMirror(gitlabRepo: Project, config: GitrOptions): Promise<boolean> {
    // has mirrors setup already?
    const { id } = gitlabRepo
    const headers = GitLab.getHeaders(config)
    const mirrors = await GitLab.getMirrors(id, headers)
    if (mirrors.length === 0) {
      return true
    }

    const githubURL = GitHub.getGitHubMirrorURLWithoutAuth(config.githubPath)
    const mirror = mirrors.find((mirrorData) => mirrorData.url.includes(githubURL))
    if (!mirror) {
      return true
    }

    if (!mirror.enabled) {
      return true
    }

    return GitLab.disableMirror(id, mirror.id, headers)
  }

  /**
   * Disable GitLab mirror
   *
   * @param {number} projectId GitLab project ID
   * @param {number} mirrorId GitLab mirror ID
   * @param {Headers} headers HTTP headers
   * @returns {Promise<boolean>} Returns true if mirror is disabled or returns false if failed to disable
   */
  private static async disableMirror(projectId: number, mirrorId: number, headers: Headers): Promise<boolean> {
    Logger.log('GitLab: Disabling GitHub push mirror')

    try {
      const response = await fetch(`https://gitlab.com/api/v4/projects/${projectId}/remote_mirrors/${mirrorId}`, {
        body: JSON.stringify({ enabled: false }),
        headers,
        method: 'PUT',
      })
      const data = ((await response.json()) as unknown) as RemoteMirror

      return !data.enabled
    } catch (error) {
      Logger.error('Failed to disable GitHub push mirror')
      Logger.error(error)

      return false
    }
  }
  /**
   * Get Headers
   *
   * @param {GitrOptions} config configurations
   * @returns {Headers} Returns headers data for GitLab API communication
   */
  private static getHeaders(config: GitrOptions): Headers {
    const headers = new Headers()
    headers.append('PRIVATE-TOKEN', config.gitlabToken)
    headers.append('Content-Type', 'application/json')

    return headers
  }

  /**
   * Get mirrors for the project from gitlab.com
   *
   * @param {number} projectId Project ID
   * @param {Headers} headers Headers instance with authentication
   * @returns {Promise<RemoteMirror[]>} Returns array of remote mirrors of the project
   */
  private static async getMirrors(projectId: number, headers: Headers): Promise<readonly RemoteMirror[]> {
    const response = await fetch(`https://gitlab.com/api/v4/projects/${projectId}/remote_mirrors`, {
      headers,
      method: 'GET',
    })

    return response.json()
  }

  /**
   * Enable repository mirroring of an existing mirror
   *
   * @param {number} projectId GitLab project ID
   * @param {number} mirrorId GitLab mirror ID
   * @param {Headers} headers HTTP headers to perform fetch request
   * @returns {Promise<boolean>} Returns true if mirror is enabled or false otherwise
   */
  private static async enableMirror(projectId: number, mirrorId: number, headers: Headers): Promise<boolean> {
    Logger.log('GitLab: Enabling GitHub push mirror')

    try {
      const response = await fetch(`https://gitlab.com/api/v4/projects/${projectId}/remote_mirrors/${mirrorId}`, {
        body: JSON.stringify({ enabled: true }),
        headers,
        method: 'PUT',
      })
      const data = ((await response.json()) as unknown) as RemoteMirror

      return data.enabled
    } catch (error) {
      Logger.error('Failed to enable GitHub push mirror')
      Logger.error(error)

      return false
    }
  }

  /**
   * Is given groupPath already exist in gitlab.com?
   *
   * @param {string} groupPath Group full path
   * @param {Group[]} groups Current gitlab group data
   * @returns {object|false} Returns the group data if group exists otherwise returns false
   */
  private static groupExists(groupPath: string, groups: readonly Group[]): object | false {
    // eslint-disable-next-line functional/no-loop-statement
    for (const group of groups) {
      if (group.full_path === groupPath) {
        return group
      }
    }

    return false
  }

  /**
   * Create or update GitLab project and required subgroups.
   *
   * If the project already exist it will perform an update to the project. To perform an update
   * the project should be inside the same group.
   *
   * @param {GitLabPathData} data GitLab path data
   * @param {GitrOptions} config Gitr options
   * @returns {Promise<Project>} Returns GitLab Project data
   */
  public async createProject(data: GitLabPathData, config: GitrOptions): Promise<Project> {
    Logger.log(`GitLab: Creating project "${data.repoSlug}"`)

    let group

    let isGitLabUsername = false
    // is there only a single group?
    if (data.groups.length.toString() === '1') {
      // is it exactly a group or gitlab username of the current user?
      isGitLabUsername = await this.isUsername(data.groups[0])
    }

    // create groups
    if (!isGitLabUsername && data.groups.length > 0) {
      group = await this.createGroups(data.groups, config)
    }

    let projectVisibility = config.public ? 'public' : 'private'
    if (group.visibility === 'private' && config.public) {
      Logger.warn('GitLab: Parent group\'s visibility is "private", so project visibility cannot be "public"')
      Logger.warn('GitLab: Changing visibility to "private"')
      projectVisibility = 'private'
    }

    const projectConfig: IProjectConfig = {
      description: config.description,
      name: config.gitlabTitle,
      path: data.repoSlug,
      visibility: projectVisibility,
      wiki_enabled: config.wiki,
      // @TODO: wiki_access_level = disabled is not working from the tests
      // wiki_access_level: config.wiki && config.public ? 'enabled' : config.wiki ? 'private' : 'disabled'
    }

    // is repository exist?
    const project = await this.isProjectExists(data.repoSlug, config.gitlabPath, group)
    if (typeof project === 'object') {
      Logger.warn(`GitLab: Project "${config.gitlabPath}" already exists`)

      // update project data
      if (config.overwrite) {
        await this.api.Projects.edit(project.id, projectConfig)
        Logger.log(`GitLab: "${config.gitlabPath}" project updated`)
      }

      // return the project data
      return project
    }

    // all good, create GitLab project repository

    Logger.debug('GitLab project configurations', projectConfig)
    if (group) {
      const projectConfigWithNamespace = {
        ...projectConfig,
        namespace_id: group.id,
      }
      const projectData = await this.api.Projects.create(projectConfigWithNamespace)
      Logger.log(`GitLab: "${config.gitlabPath}" project created`)

      return projectData
    }

    const projectData = await this.api.Projects.create(projectConfig)
    Logger.log(`GitLab: "${config.gitlabPath}" project created`)

    return projectData
  }

  /**
   * Is username the current user's login?
   *
   * @param {string} username String to check for username
   * @returns {boolean} true if given string is username or false otherwise
   */
  public async isUsername(username: string): Promise<boolean> {
    try {
      const currentUser = await this.api.Users.current()

      return username === currentUser.username
    } catch (error) {
      Logger.error(error)

      return false
    }
  }

  /**
   * Is project already exist in the gitlab.com?
   *
   * @param {string} project Project name
   * @param {string} projectPath GitLab full path
   * @param {ILooseObject} group Group data
   * @returns {Promise<Project|false>} Returns project data if found or returns false
   */
  private async isProjectExists(project: string, projectPath: string, group?: ILooseObject): Promise<Project | false> {
    const projects = group ? await this.api.GroupProjects.all(group.id) : await this.api.Search.all('projects', project)

    if (!projects) {
      return false
    }

    // eslint-disable-next-line functional/no-loop-statement
    for (const proj of projects) {
      if (proj.path_with_namespace === projectPath) {
        return proj
      }
    }

    return false
  }

  /**
   * Get groups data from the gitlab.com
   *
   * @returns {Promise<Group[]>} Returns groups data
   */
  private async getGroups(): Promise<readonly Group[]> {
    let groups: readonly Group[] = []
    let next = true
    // eslint-disable-next-line functional/no-loop-statement
    do {
      Logger.info('GitLab: Fetching groups data')
      const { data, paginationInfo } = await this.api.Groups.all({ showExpanded: true })

      // merge found group data to groups array
      groups = [...groups, ...data]
      ;({ next } = paginationInfo)
    } while (next)

    return groups
  }

  /**
   * Create sub group
   *
   * @param {ILooseObject} parent Parent Group
   * @param {string} name Group name
   * @param {string} groupPath Group path
   * @param {boolean} isPublic Group visibility is public?
   * @returns {Promise<Group>} Returns group data
   */
  private async createSubGroup(
    parent: ILooseObject,
    name: string,
    groupPath: string,
    isPublic: boolean
  ): Promise<Group> {
    const groupConfig: IGroupConfig = {
      parent_id: parent.id as number,
      // group visibility can be public only if the parent group's visibility is public
      visibility: parent.visibility === 'public' && isPublic ? 'public' : 'private',
    }

    const group = await this.api.Groups.create(name, groupPath, groupConfig)

    return group
  }

  /**
   * Create GitLab groups for project
   *
   * It will create groups including all nested groups. But the root group must
   * exist in the GitLab.com. Otherwise function will throw exception
   *
   * @param {string[]} groups GitLab groups
   * @param {GitrOptions} config Gitr configurations
   * @returns {Promise<Group>} Returns created group data
   */
  private async createGroups(groups: readonly string[], config: GitrOptions): Promise<Group> {
    // get current groups
    const gitlabGroups = await this.getGroups()

    const groupsOriginal = [...groups]

    let groupPath = groupsOriginal.join('/')
    let parentGroup
    // eslint-disable-next-line functional/prefer-readonly-type
    const groupsToCreate: string[] = []

    // find the max depth sub group that already exist
    // eslint-disable-next-line functional/no-loop-statement
    while (groupPath.length > 0) {
      parentGroup = GitLab.groupExists(groupPath, gitlabGroups)
      if (parentGroup) {
        break
      }

      // get next depth of the group
      // eslint-disable-next-line functional/immutable-data
      const groupToCreate = groupsOriginal.pop()
      if (groupToCreate) {
        // eslint-disable-next-line functional/immutable-data
        groupsToCreate.unshift(groupToCreate)
      }

      // next group path
      groupPath = groupsOriginal.join('/')
    }

    // need to create any subgroups?
    if (groupsToCreate.length > 0) {
      // is parent group exist
      if (!parentGroup) {
        throw new Error(
          `Unable to create the root group of GitLab.
          See https://gitlab.com/gitlab-org/gitlab/-/issues/244345.
          Please create the "${groups[0]}" group manually by logging into gitlab.com`
        )
      }

      // create all the sub groups
      // eslint-disable-next-line functional/no-loop-statement
      while (groupsToCreate.length > 0) {
        // eslint-disable-next-line functional/immutable-data
        const nextGroup = groupsToCreate.shift()
        if (!nextGroup) {
          break
        }
        parentGroup = await this.createSubGroup(parentGroup, nextGroup, nextGroup, config.public)
      }
    }

    return parentGroup
  }
}
