/* eslint-disable functional/no-loop-statement */
import test from 'ava'

import { GitHub } from './github'

test('parsePath throws error for blank paths', (t) => {
  for (const pathString of ['', '  ']) {
    t.throws(
      () => {
        GitHub.parsePath(pathString)
      },
      {
        instanceOf: Error,
        message: 'path contains empty repository slug',
      }
    )
  }
})

test('parsePath throws error for invalid number of path segments', (t) => {
  for (const pathString of ['/', 'username/']) {
    t.throws(
      () => {
        GitHub.parsePath(pathString)
      },
      {
        instanceOf: Error,
        message: 'path contains empty repository slug',
      }
    )
  }
  for (const pathString of ['//', 'my/repo/', 'my/repo/invalid']) {
    t.throws(
      () => {
        GitHub.parsePath(pathString)
      },
      {
        instanceOf: Error,
        message: 'path must contain only username/organization and repository slug',
      }
    )
  }
  t.throws(
    () => {
      GitHub.parsePath('/slug')
    },
    {
      instanceOf: Error,
      message: 'path contains empty owner',
    }
  )
})

test('getGitHubMirrorURLWithoutAuth should return github url', (t) => {
  const repo = 'my-repository'
  t.deepEqual(GitHub.getGitHubMirrorURLWithoutAuth(repo), `github.com/${repo}.git`)
})
