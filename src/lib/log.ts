/* eslint-disable functional/functional-parameters */
import { default as Pino } from 'pino'

const pinoChildMinLength = 1

export const pinoLog = Pino({
  enabled: !process.env.NODE_ENV || process.env.NODE_ENV.toLowerCase() !== 'test',
  prettyPrint: {
    ignore: 'hostname,pid',
    translateTime: 'SYS:HH:MM:ss.l o',
  },
})

/**
 * Shared logger used instead of console.log
 */
export class Logger {
  /**
   * Debug log level
   *
   * @param {...any} arguments_ Arguments to log
   */
  public static debug(...arguments_): void {
    if (arguments_ && arguments_.length > pinoChildMinLength) {
      const child = pinoLog.child({ debug: arguments_[1] })
      child.debug(arguments_[0])
    } else {
      pinoLog.debug(...arguments_)
    }
  }

  /**
   * Error log level
   *
   * @param {...any} arguments_ Arguments to log
   */
  public static error(...arguments_): void {
    if (arguments_ && arguments_.length > pinoChildMinLength) {
      const child = pinoLog.child({ data: arguments_[1], error: arguments_[1].toString() })
      child.error(arguments_[0])
    } else {
      pinoLog.error(...arguments_)
    }
  }

  /**
   * Info log level
   *
   * @param {...any} arguments_ Arguments to log
   */
  public static info(...arguments_): void {
    if (arguments_ && arguments_.length > pinoChildMinLength) {
      const child = pinoLog.child({ info: arguments_[1] })
      child.info(arguments_[0])
    } else {
      pinoLog.info(...arguments_)
    }
  }

  /**
   * Alias for info log level
   *
   * @param {...any} arguments_ Arguments to log
   */
  public static log(...arguments_): void {
    Logger.info(...arguments_)
  }

  /**
   * Warning log level
   *
   * @param {...any} arguments_ Arguments to log
   */
  public static warn(...arguments_): void {
    if (arguments_ && arguments_.length > pinoChildMinLength) {
      const child = pinoLog.child({ warning: arguments_[1] })
      child.warn(arguments_[0])
    } else {
      pinoLog.warn(...arguments_)
    }
  }
}
