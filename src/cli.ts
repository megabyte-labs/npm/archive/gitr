import fs from 'fs'
import path from 'path'

import jsonfile from 'jsonfile'
import optionator_ from 'optionator'

import { Gitr } from './app'
import { CLI_OPTIONS } from './constants/cli-options.constant'
import { Logger } from './lib/log'

const optionator = optionator_(CLI_OPTIONS)

const EXIT_0 = 0
const EXIT_1 = 1

/**
 * Delegate CLI commands to appropriate logic
 *
 * @param {any} argz CLI arguments
 * @returns {number} Returns the exit status
 */
export async function cli(argz: unknown): Promise<number> {
  const options = optionator.parseArgv(argz)

  const configPath = options.config

  if (options.help) {
    // eslint-disable-next-line no-console
    console.log(optionator.generateHelp())
  }
  const _path = path.resolve(process.cwd(), configPath)

  // is path exist?
  if (!fs.existsSync(_path)) {
    Logger.error(`The path to the configuration file you specified (${_path}) does not exist`)

    return EXIT_1
  }

  // is path directory?
  if (fs.lstatSync(_path).isDirectory()) {
    Logger.error(
      'The path to the configuration file you specified appears to be a directory. A JSON file was expected.'
    )

    return EXIT_1
  }

  // is valid json file?
  try {
    const config = await jsonfile.readFile(_path)
    await Gitr.run(config, _path)

    return EXIT_0
  } catch {
    Logger.error(`Failed to read ${_path}. Make sure the file contains valid JSON document`)

    return EXIT_1
  }
}
